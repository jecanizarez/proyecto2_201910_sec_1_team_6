package model.vo;

import java.time.LocalDateTime;
import java.util.Comparator;

import model.logic.ManejoFechaHora;
/**
 * Representation of a Trip object
 */
public class VOMovingViolations implements Comparable <VOMovingViolations> {

	private int objectId;
	private String location;
	private String ticketIssueDate;
	private double totalPaid;
	private String accidentIndicator;
	private String violationDescription;
	private String streetSegId;
	private String addressId;
	private double xCoord;
	private double yCoord;
	private double fineAmt;
	private double penalty1;
	private double penalty2;
	private String violationCode;
	private LocalDateTime date;
	private int addressIdi;
	@Override	
	public String toString() 
	{
		return "VOMovingViolations [objectId()=" + objectId() + ",\n getLocation()=" + getLocation()
		+ ",\n getTicketIssueDate()=" + getTicketIssueDate() + ",\n getTotalPaid()=" + getTotalPaid()
		+ ",\n getAccidentIndicator()=" + getAccidentIndicator() + ",\n getViolationDescription()="
		+ getViolationDescription() + ",\n getStreetSegId()=" + getStreetSegId() + ",\n getAddressId()="
		+ getAddressId() + "]\n\n";
	}

	public VOMovingViolations(int pId, String pLocation, String pTicketIssueDate, double pTotalPaid, String pAccidentIndicator, String pViolationDescription, String pAddressID, String pstreetID, Double pXCoord, Double pYCoord, double pFineAmt, double pPenalty1, double pPenalty2, String pViolationCode)
	{
		objectId=pId;
		location=pLocation;
		ticketIssueDate=pTicketIssueDate;
		totalPaid=pTotalPaid;
		accidentIndicator=pAccidentIndicator;
		violationDescription=pViolationDescription;
		addressId = pAddressID;
		streetSegId = pstreetID;
		xCoord=pXCoord;
		yCoord=pYCoord;
		fineAmt=pFineAmt;
		penalty1=pPenalty1;
		penalty2=pPenalty2;
		violationCode=pViolationCode;
		date=ManejoFechaHora.convertirFecha_Hora_LDT(ticketIssueDate);
		addressIdi=!pAddressID.isEmpty()?Integer.parseInt(pAddressID):0;
	}

	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() {
		// TODO Auto-generated method stub
		return objectId;
	}	


	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return location;
	}

	public LocalDateTime getDate()
	{
		return date;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {
		// TODO Auto-generated method stub
		return ticketIssueDate;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public double getTotalPaid() {
		// TODO Auto-generated method stub
		return totalPaid;
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		// TODO Auto-generated method stub
		return accidentIndicator;
	}

	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return violationDescription;
	}

	public String getStreetSegId() {
		return streetSegId;
	}

	public String getAddressId() {
		return addressId;
	}
	
	public int getAddressIdi()
	{
		return addressIdi;
	}

	public double getXCoord()
	{
		return xCoord;
	}

	public double getYCoord()
	{
		return yCoord;
	}

	public String getViolationCode()
	{
		return violationCode;
	}

	@Override
	public int compareTo(VOMovingViolations arg0) 
	{
		if(xCoord-arg0.getXCoord()!=0.0)
			return (xCoord-arg0.getXCoord()>0.0?1:-1);
		else if(yCoord-arg0.getYCoord()!=0.0)
			return (yCoord-arg0.getYCoord()>0.0?1:-1);
		else 
			return 0;
	}

	public double totalAPagar()
	{
		return totalPaid+fineAmt+penalty1+penalty2;
	}

	public static class comparatorViolationCode implements Comparator <VOMovingViolations>
	{
		public int compare(VOMovingViolations arg0, VOMovingViolations arg1 )
		{
			return arg0.getViolationCode().compareTo(arg1.getViolationCode());
		}
	}

	public static class comparatorLocalDateTime implements Comparator <VOMovingViolations>
	{
		public int compare(VOMovingViolations arg0, VOMovingViolations arg1 )
		{
			return arg0.getDate().compareTo(arg1.getDate());
		}
	}

	public static class comparatorHour implements Comparator <VOMovingViolations>
	{
		public int compare(VOMovingViolations arg0, VOMovingViolations arg1 )
		{
			return ((arg0.getDate().getHour()*60)+arg0.getDate().getMinute())-((arg1.getDate().getHour()*60)+arg1.getDate().getMinute());
		}
	}
	public static class comparatorCoord implements Comparator <VOMovingViolations>
	{
		public int compare(VOMovingViolations arg0, VOMovingViolations arg1 )
		{
			if(arg0.getXCoord()-arg1.getXCoord()!=0.0)
			{
				return arg0.getXCoord()-arg1.getXCoord()>0.0?1:-1;
			}
			else if(arg0.getYCoord()-arg1.getYCoord()!=0.0) 
			{
				return arg0.getYCoord()-arg1.getYCoord()>0.0?1:-1;
			}
			else
				return 0;
		}
	}

	public static class comparatorAddressID implements Comparator <VOMovingViolations>
	{
		public int compare(VOMovingViolations arg0, VOMovingViolations arg1 )
		{
			return arg0.getAddressIdi()-arg1.getAddressIdi();
		}
	}
}