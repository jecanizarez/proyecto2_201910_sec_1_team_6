package model.data_structures;

import java.util.Iterator;

public class MaxHeapCP<T extends Comparable<T>> implements IColaPrioridad<T> 
{
	private T[] elements;                  
	private int numElementos;                  

	public MaxHeapCP(int initCapacity) 
	{
		elements = (T[]) new Comparable[initCapacity + 1];
		numElementos = 0;
	}

	public MaxHeapCP(T[] elementos) 
	{
		numElementos = elementos.length;
		elements = (T[]) new Comparable[elementos.length + 1];
		for (int i = 0; i < numElementos; i++)
			elements[i+1] = elementos[i];
		for (int k = numElementos/2; k >= 1; k--)
			sink(k);
	}

	public boolean esVacia() {
		return numElementos == 0;
	}

	public int darNumElementos() {
		return numElementos;
	}

	public T max() {
		if (esVacia()) 
			return null;

		return elements[1];
	}

	private void resize(int capacity) {
		T[] temp = (T[]) new Comparable[capacity];
		for (int i = 1; i <= numElementos; i++) {
			temp[i] = elements[i];
		}
		elements = temp;
	}

	public void agregar(T x) {
		if (numElementos == elements.length - 1) resize(2 * elements.length);

		elements[++numElementos] = x;
		swim(numElementos);
	}

	public T delMax() {
		if (esVacia()) return null;
		T max = elements[1];
		exch(1, numElementos--);
		sink(1);
		elements[numElementos+1] = null;
		if ((numElementos > 0) && (numElementos == (elements.length - 1) / 4)) resize(elements.length / 2);
		return max;
	}

	private void swim(int k) {
		while (k > 1 && less(k/2, k)) {
			exch(k, k/2);
			k = k/2;
		}
	}

	private void sink(int k) {
		while (2*k <= numElementos) {
			int j = 2*k;
			if (j < numElementos && less(j, j+1)) j++;
			if (!less(k, j)) break;
			exch(k, j);
			k = j;
		}
	}

	public T darElemento(int i)
	{
		return elements[i];
	}

	private boolean less(int i, int j)
	{
		return elements[i].compareTo(elements[j])<0;
	}

	private void exch(int i, int j) 
	{
		T swap = elements[i];
		elements[i] = elements[j];
		elements[j] = swap;
	}


	private boolean isMaxHeap(int k) {
		if (k > numElementos) return true;
		int left = 2*k;
		int right = 2*k + 1;
		if (left  <= numElementos && less(k, left))  return false;
		if (right <= numElementos && less(k, right)) return false;
		return isMaxHeap(left) && isMaxHeap(right);
	}

	public Iterator<T> iterator() {
		return new HeapIterator();
	}

	private class HeapIterator implements Iterator<T> 
	{
		private MaxHeapCP<T> copy;

		public HeapIterator() {

			for (int i = 1; i <= numElementos; i++)
				copy.agregar(elements[i]);
		}

		public boolean hasNext()  
		{ 
			return !copy.esVacia();                     
		}

		public T next() {
			if (!hasNext()) return null;
			return copy.delMax();
		}
	}
}