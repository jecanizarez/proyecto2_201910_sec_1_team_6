package model.data_structures;
import java.util.Comparator;
import model.vo.VOMovingViolations;

public class Sort {

	private static Comparator<VOMovingViolations> comparadorActual;
	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 * @param hi 
	 * @param mid 
	 * @param lo 
	 * @param aux 
	 */
	public static void ordenarMergeSort( ArregloDinamico<VOMovingViolations> datos, ArregloDinamico<VOMovingViolations> aux, int lo, int mid, int hi ) 
	{
		for (int k = lo; k <= hi; k++)
		
		{
			aux.cambiar(datos.darElemento(k),k);
		}
		
		int i = lo;
		int j = mid+1;

		for (int k = lo; k <= hi; k++)
		{
			if      (i > mid)
				datos.cambiar(aux.darElemento(j++), k);
			else if (j > hi)             
				datos.cambiar(aux.darElemento(i++), k);
			else if (less(aux.darElemento(j), aux.darElemento(i))) 
				datos.cambiar(aux.darElemento(j++), k);
			else                      
				datos.cambiar(aux.darElemento(i++), k);
		}
	}
	private static void sortM(ArregloDinamico<VOMovingViolations>datos, ArregloDinamico<VOMovingViolations>aux, int lo, int hi)
	{
		if (hi<=lo)
			return;
		int mid=lo+(hi-lo)/2;
		sortM(datos,aux,lo,mid);
		sortM(datos,aux,mid+1,hi);
		ordenarMergeSort(datos,aux,lo,mid,hi);
	}

	public static void sortM(ArregloDinamico<VOMovingViolations>datos, Comparator<VOMovingViolations>entrada)
	{
		comparadorActual = entrada;
		ArregloDinamico<VOMovingViolations>aux=new ArregloDinamico<VOMovingViolations>(datos.darTamano());
		sortM(datos,aux,0,datos.darTamano()-1);
	}



	/**
	 * Comparar 2 objetos usando la comparacion "natural" de su clase
	 *param v primer objeto de comparacion
	 * @param w segundo objeto de comparacion
	 * @return true si v es menor que w usando el metodo compareTo. false en caso contrario.
	 */
	private static boolean less(VOMovingViolations v, VOMovingViolations w)
	{
		return comparadorActual.compare(v, w)<0;
	}

	public static void invertirMuestra( VOMovingViolations[ ] datos )
	{
		VOMovingViolations[] muestraInvertida = new VOMovingViolations[ datos.length ];
		int j = datos.length-1;
		for ( int i = 0 ; i < datos.length-1 ; i++ )
		{
			muestraInvertida[i] = datos[j];
			j--;	
		}	
		datos = muestraInvertida;
	}


}