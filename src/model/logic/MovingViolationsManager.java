package model.logic;

import java.io.FileReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.opencsv.CSVReader;

import model.vo.EstadisticaInfracciones;
import model.vo.EstadisticasCargaInfracciones;
import model.vo.InfraccionesFecha;
import model.vo.InfraccionesFechaHora;
import model.vo.InfraccionesFranjaHoraria;
import model.vo.InfraccionesFranjaHorariaViolationCode;
import model.vo.InfraccionesLocalizacion;
import model.vo.InfraccionesViolationCode;
import model.vo.LocalizacionInfracciones2;
import model.vo.VOMovingViolations;
import model.data_structures.*;

public class MovingViolationsManager {

	//TODO Definir atributos necesarios
	private enum Meses
	{
		January(0), February(0), March(0), April(0), May(0), June(0), July(0), August(0), September(0), October(0), November(0), December(0);

		private int infracciones;

		private Meses(int cantidad)
		{ 
			this.infracciones = cantidad;
		}

		private void contar()
		{ 
			this.infracciones++; 
		}

		private int darInfracciones()
		{ 
			return infracciones; 
		}
	}

	private EstadisticasCargaInfracciones estadisticasCarga;
	private ArregloDinamico<VOMovingViolations> datos; 
	private ArregloDinamico<VOMovingViolations> infraccionesLocalizacion;
	private ArregloDinamico<VOMovingViolations> infraccionesFecha;
	private MaxHeapCP<InfraccionesViolationCode> codes;
	private ArregloDinamico<VOMovingViolations> infraccionesHora;
	private ArregloDinamico<VOMovingViolations> infraccionesAddress;
	private ArregloDinamico<VOMovingViolations> infraccionesCodes;
	private MaxHeapCP<InfraccionesLocalizacion> coordenadas;
	/**
	 * Metodo constructor
	 */
	public MovingViolationsManager()
	{
		//TODO inicializar los atributos
		datos=new ArregloDinamico<VOMovingViolations>(670000);
	}

	/**
	 * Cargar las infracciones de un semestre de 2018
	 * @param numeroSemestre numero del semestre a cargar (1 o 2)
	 * @return objeto con el resultado de la carga de las infracciones
	 */
	public EstadisticasCargaInfracciones loadMovingViolations(int numeroSemestre) {
		CSVReader reader;
		String[] nextLine;
		int[] infraccionesxMes=new int[6];
		double[] miniMax=new double[4];
		double miny=Double.MAX_VALUE;
		double minx=Double.MAX_VALUE;
		double maxx=0.0;
		double maxy=0.0;
		try
		{
			double coordenadax=0.0; 
			double coordenaday=0.0;
			for (Meses mes : Meses.values())
			{
				if(mes.ordinal()>=(numeroSemestre-1)*6 && mes.ordinal()<numeroSemestre*6)
				{
					reader = new CSVReader(new FileReader("./data/Moving_Violations_Issued_in_"+mes+"_2018.csv"));
					nextLine=reader.readNext();  
					if(mes.ordinal() >8 )
					{

						while ((nextLine = reader.readNext()) != null) 
						{			
							coordenadax = Double.parseDouble(nextLine[5]);

							coordenaday = Double.parseDouble(nextLine[6]);

							if(coordenadax>maxx)
							{
								maxx=coordenadax;
							}
							if(coordenadax<minx)
							{
								minx=coordenadax;
							}
							if(coordenaday>maxy)
							{
								maxy=coordenaday;
							}
							if(coordenaday<miny)
							{
								miny=coordenaday;
							}
							datos.agregar(new VOMovingViolations(Integer.parseInt(nextLine[0]), nextLine[2],nextLine[14], Double.parseDouble(nextLine[9]),nextLine[12],nextLine[16],nextLine[3],nextLine[4],coordenadax,coordenaday,(nextLine[8]!="" && nextLine[8]!=null && !nextLine[8].isEmpty())?Double.parseDouble(nextLine[8]):0 ,(nextLine[10]!="" && nextLine[10]!=null && !nextLine[10].isEmpty())?Double.parseDouble(nextLine[10]):0  ,(nextLine[11]!="" && nextLine[11]!=null && !nextLine[11].isEmpty())?Double.parseDouble(nextLine[11]):0, nextLine[15]));
							mes.contar();
						}
						reader.close();
					}
					else
					{	
						while ((nextLine = reader.readNext()) != null) 
						{				
							coordenadax = Double.parseDouble(nextLine[5]);

							coordenaday = Double.parseDouble(nextLine[6]);

							if(coordenadax>maxx)
							{
								maxx=coordenadax;
							}
							if(coordenadax<minx)
							{
								minx=coordenadax;
							}
							if(coordenaday>maxy)
							{
								maxy=coordenaday;
							}
							if(coordenaday<miny)
							{
								miny=coordenaday;
							}
							datos.agregar(new VOMovingViolations(Integer.parseInt(nextLine[0]), nextLine[2],nextLine[13], Double.parseDouble(nextLine[9]),nextLine[12],nextLine[15],nextLine[3],nextLine[4],coordenadax,coordenaday,(nextLine[8]!="" && nextLine[8]!=null && !nextLine[8].isEmpty())?Double.parseDouble(nextLine[8]):0 ,(nextLine[10]!="" && nextLine[10]!=null && !nextLine[10].isEmpty())?Double.parseDouble(nextLine[10]):0  ,(nextLine[11]!=null && nextLine[11]!="" && !nextLine[11].isEmpty())?Double.parseDouble(nextLine[11]):0, nextLine[14] ));
							mes.contar();
						}
						reader.close();
					}
					infraccionesxMes[mes.ordinal()%6]=mes.darInfracciones();
				}

			}
		}
		catch(Exception e)
		{ 
			e.printStackTrace(); 
		}

		miniMax[0]=minx;
		miniMax[1]=miny;
		miniMax[2]=maxx;
		miniMax[3]=maxy;
		estadisticasCarga=new EstadisticasCargaInfracciones(datos.darTamano(),infraccionesxMes,miniMax);
		infraccionesLocalizacion=datos;
		Sort.sortM(infraccionesLocalizacion, new VOMovingViolations.comparatorCoord());
		infraccionesFecha=datos;
		Sort.sortM(infraccionesFecha, new VOMovingViolations.comparatorLocalDateTime());
		infraccionesHora=datos;
		Sort.sortM(infraccionesHora, new VOMovingViolations.comparatorHour());
		infraccionesAddress=datos;
		Sort.sortM(infraccionesAddress, new VOMovingViolations.comparatorAddressID());
		infraccionesCodes = datos;
		Sort.sortM(infraccionesCodes, new VOMovingViolations.comparatorViolationCode());

		return estadisticasCarga;
	}

	/**
	 * Requerimiento 1A: Obtener el ranking de las N franjas horarias
	 * que tengan m�s infracciones. 
	 * @param int N: N�mero de franjas horarias que tienen m�s infracciones
	 * @return Cola con objetos InfraccionesFranjaHoraria
	 */
	public IQueue<InfraccionesFranjaHoraria> rankingNFranjas(int N)
	{
		Sort.sortM(datos, new VOMovingViolations.comparatorHour());
		LocalTime fecha=datos.darElemento(0).getDate().toLocalTime();
		Queue<VOMovingViolations> violaciones = new Queue<VOMovingViolations>();
		MaxHeapCP<InfraccionesFranjaHoraria> heapFranjaH = new MaxHeapCP<InfraccionesFranjaHoraria>(1500);
		int i;
		for( i=0; i < datos.darTamano(); i++)
		{
			if(datos.darElemento(i).getDate().toLocalTime().getHour()-fecha.getHour()==0)
			{
				violaciones.enqueue(datos.darElemento(i));	
			}
			else
			{
				InfraccionesFranjaHoraria dato = new InfraccionesFranjaHoraria(fecha, datos.darElemento(i-1).getDate().toLocalTime(),violaciones);
				heapFranjaH.agregar(dato);
				fecha = datos.darElemento(i).getDate().toLocalTime();
				violaciones = new Queue<VOMovingViolations>();
				violaciones.enqueue(datos.darElemento(i));
			}
		}
		InfraccionesFranjaHoraria dato = new InfraccionesFranjaHoraria(fecha, datos.darElemento(i-1).getDate().toLocalTime(),violaciones);
		heapFranjaH.agregar(dato);

		Queue<InfraccionesFranjaHoraria> retorno = new Queue<InfraccionesFranjaHoraria>();
		for(int j = 0; j < N; j++)
		{
			retorno.enqueue(heapFranjaH.delMax());
		}
		Queue<InfraccionesFranjaHoraria>retornar=new Queue<InfraccionesFranjaHoraria>();
		for (int m=0;m<N;m++)
		{
			retornar.enqueue(retorno.dequeue());
		}
		return retornar;				
	}

	/**
	 * Requerimiento 2A: Consultar  las  infracciones  por
	 * Localizaci�n  Geogr�fica  (Xcoord, Ycoord) en Tabla Hash.
	 * @param  double xCoord : Coordenada X de la localizacion de la infracci�n
	 *			double yCoord : Coordenada Y de la localizacion de la infracci�n
	 * @return Objeto InfraccionesLocalizacion
	 */
	public InfraccionesLocalizacion consultarPorLocalizacionHash(double xCoord, double yCoord)
	{
		Sort.sortM(datos, new VOMovingViolations.comparatorCoord());
		VOMovingViolations muestra=datos.darElemento(0);
		Queue<VOMovingViolations> violaciones = new Queue<VOMovingViolations>();
		SeparateChainingHash<InfraccionesLocalizacion,Queue<VOMovingViolations>> tablaHash=new SeparateChainingHash<InfraccionesLocalizacion,Queue<VOMovingViolations>>(25);
		for(int i=0;i<datos.darTamano();i++)
		{
			if(datos.darElemento(i).getXCoord()==muestra.getXCoord() && datos.darElemento(i).getYCoord()==muestra.getYCoord())
			{
				violaciones.enqueue(datos.darElemento(i));
			}
			else
			{
				tablaHash.put(new InfraccionesLocalizacion(muestra.getXCoord(),muestra.getYCoord(),muestra.getLocation(),muestra.getAddressId(),muestra.getStreetSegId(),violaciones), violaciones);
				muestra=datos.darElemento(i);
				violaciones=new Queue<VOMovingViolations>();
				violaciones.enqueue(datos.darElemento(i));
			}
		}
		tablaHash.put(new InfraccionesLocalizacion(muestra.getXCoord(),muestra.getYCoord(),muestra.getLocation(),muestra.getAddressId(),muestra.getStreetSegId(),violaciones), violaciones);
		for(InfraccionesLocalizacion e : tablaHash.keys())
		{
			if(e.getXcoord()== xCoord && e.getYcoord() == yCoord)
			{
				return e;
			}
		}
		return null;	
	}

	/**
	 * Requerimiento 3A: Buscar las infracciones por rango de fechas
	 * @param  LocalDate fechaInicial: Fecha inicial del rango de b�squeda
	 * 		LocalDate fechaFinal: Fecha final del rango de b�squeda
	 * @return Cola con objetos InfraccionesFecha
	 */
	public IQueue<InfraccionesFecha> consultarInfraccionesPorRangoFechas(LocalDate fechaInicial, LocalDate fechaFinal)
	{
		Queue<VOMovingViolations> violaciones = new Queue<VOMovingViolations>();
		RedBlackTree<InfraccionesFecha,Queue<VOMovingViolations>> arbol=new RedBlackTree<InfraccionesFecha,Queue<VOMovingViolations>>();
		LocalDate fecha=infraccionesFecha.darElemento(0).getDate().toLocalDate();
		for(int i=0;i<infraccionesFecha.darTamano();i++)
		{
			if(infraccionesFecha.darElemento(i).getDate().toLocalDate().compareTo(fecha)==0)
			{
				violaciones.enqueue(infraccionesFecha.darElemento(i));
			}
			else
			{
				arbol.put(new InfraccionesFecha(violaciones,fecha), violaciones);
				fecha=infraccionesFecha.darElemento(i).getDate().toLocalDate();
				violaciones=new Queue<VOMovingViolations>();
				violaciones.enqueue(infraccionesFecha.darElemento(i));
			}
		}
		arbol.put(new InfraccionesFecha(violaciones,fecha), violaciones);
		Queue <InfraccionesFecha> sacada=new Queue<InfraccionesFecha>();
		for(InfraccionesFecha e : arbol.keys())
		{
			if(e.darFecha().compareTo(fechaInicial)>=0 && e.darFecha().compareTo(fechaFinal)<=0)
			{
				sacada.enqueue(e);
			}
			else if(e.darFecha().compareTo(fechaFinal)>0)
			{
				break;
			}
		}
		int t=sacada.size();
		Queue <InfraccionesFecha> retorno=new Queue<InfraccionesFecha>();
		for(int k=0;k<t;k++)
		{
			retorno.enqueue(sacada.dequeue());
		}
		return retorno;
	}

	/**
	 * Requerimiento 1B: Obtener  el  ranking  de  las  N  tipos  de  infracci�n
	 * (ViolationCode)  que  tengan  m�s infracciones.
	 * @param  int N: Numero de los tipos de ViolationCode con m�s infracciones.
	 * @return Cola con objetos InfraccionesViolationCode con top N infracciones
	 */
	public IQueue<InfraccionesViolationCode> rankingNViolationCodes(int N)
	{
		// TODO completar
		String code = datos.darElemento(0).getViolationCode();
		Queue<VOMovingViolations> violaciones = new Queue<VOMovingViolations>();
		MaxHeapCP<InfraccionesViolationCode> respuesta = new MaxHeapCP<InfraccionesViolationCode>(1000);
		codes = new MaxHeapCP<InfraccionesViolationCode>(1000);
		violaciones.enqueue(datos.darElemento(0));
		for(int i = 1; i < datos.darTamano(); i++)
		{
			if(datos.darElemento(i).getViolationCode().equals(code))
			{
				violaciones.enqueue(datos.darElemento(i));	
			}
			else
			{

				InfraccionesViolationCode dato = new InfraccionesViolationCode(code, violaciones);
				respuesta.agregar(dato);
				codes.agregar(dato);
				code = datos.darElemento(i).getViolationCode();
				violaciones = new Queue<VOMovingViolations>();
				violaciones.enqueue(datos.darElemento(i));
			}
		}
		InfraccionesViolationCode dato = new InfraccionesViolationCode(code, violaciones);
		respuesta.agregar(dato);
		codes.agregar(dato);
		
		Queue<InfraccionesViolationCode> retorno = new Queue<InfraccionesViolationCode>();
		for(int i = 0; i < N; i++)
		{
			retorno.enqueue(respuesta.delMax());
		}
		return retorno;		
	}


	/**
	 * Requerimiento 2B: Consultar las  infracciones  por  
	 * Localizaci�n  Geogr�fica  (Xcoord, Ycoord) en Arbol.
	 * @param  double xCoord : Coordenada X de la localizacion de la infracci�n
	 *			double yCoord : Coordenada Y de la localizacion de la infracci�n
	 * @return Objeto InfraccionesLocalizacion
	 */
	public InfraccionesLocalizacion consultarPorLocalizacionArbol(double xCoord, double yCoord)
	{
		// TODO completar
		VOMovingViolations actual = infraccionesLocalizacion.darElemento(0);
		Queue<VOMovingViolations> violaciones = new Queue<VOMovingViolations>();
		violaciones.enqueue(actual);
		RedBlackTree<InfraccionesLocalizacion, Queue<VOMovingViolations>> arbol  = new RedBlackTree<InfraccionesLocalizacion, Queue<VOMovingViolations>>();
		coordenadas = new MaxHeapCP<InfraccionesLocalizacion>(2500);
		for(int i = 1; i<infraccionesLocalizacion.darTamano(); i++)
		{
			if(actual.compareTo(infraccionesLocalizacion.darElemento(i))==0)
			{
				violaciones.enqueue(infraccionesLocalizacion.darElemento(i));
			}
			else
			{
				InfraccionesLocalizacion dato = new InfraccionesLocalizacion(actual.getXCoord(), actual.getYCoord(), actual.getLocation(), actual.getAddressId(), actual.getStreetSegId(), violaciones);
				arbol.put(dato, violaciones);
				coordenadas.agregar(dato);
				actual = infraccionesLocalizacion.darElemento(i);
				violaciones = new Queue<VOMovingViolations>();
			}
		}		
		InfraccionesLocalizacion dato = new InfraccionesLocalizacion(actual.getXCoord(), actual.getYCoord(), actual.getLocation(), actual.getAddressId(), actual.getStreetSegId(), violaciones);
		arbol.put(dato, violaciones);
		coordenadas.agregar(dato);
		for(InfraccionesLocalizacion e : arbol.keys())
		{
			if(e.getXcoord()== xCoord && e.getYcoord() == yCoord)
			{
				return e;
			}
		}
		return null;

	}

	/**
	 * Requerimiento 3B: Buscar las franjas de fecha-hora donde se tiene un valor acumulado
	 * de infracciones en un rango dado [US$ valor inicial, US$ valor final]. 
	 * @param  double valorInicial: Valor m�nimo acumulado de las infracciones
	 * 		double valorFinal: Valor m�ximo acumulado de las infracciones.
	 * @return Cola con objetos InfraccionesFechaHora
	 */
	public IQueue<InfraccionesFechaHora> consultarFranjasAcumuladoEnRango(double valorInicial, double valorFinal)
	{
		// TODO completar
		//Separar por 
		int dia = infraccionesFecha.darElemento(0).getDate().getDayOfYear();
		int hora= infraccionesFecha.darElemento(0).getDate().getHour();
		VOMovingViolations actual  = infraccionesFecha.darElemento(0);

		Queue<VOMovingViolations> lista = new Queue<VOMovingViolations>();
		lista.enqueue(infraccionesFecha.darElemento(0));

		RedBlackTree<InfraccionesFechaHora, Queue<VOMovingViolations>> arbol = new RedBlackTree<InfraccionesFechaHora, Queue<VOMovingViolations>>();

		for(int i = 1; i < infraccionesFecha.darTamano(); i++)
		{
			if(infraccionesFecha.darElemento(i).getDate().getDayOfYear() == dia)
			{
				if(infraccionesFecha.darElemento(i).getDate().getHour()==hora)
				{
					lista.enqueue(infraccionesFecha.darElemento(i));
				}
				else
				{
					InfraccionesFechaHora dato = new InfraccionesFechaHora(actual.getDate(),infraccionesFecha.darElemento(i-1).getDate(),lista);
					arbol.put(dato, lista);
					lista = new Queue<VOMovingViolations>();
					hora = infraccionesFecha.darElemento(i).getDate().getHour();
				}
			}
			else
			{
				InfraccionesFechaHora dato = new InfraccionesFechaHora(actual.getDate(),infraccionesFecha.darElemento(i-1).getDate(),lista);
				arbol.put(dato, lista);
				lista = new Queue<VOMovingViolations>();
				actual = infraccionesFecha.darElemento(i);
				dia = infraccionesFecha.darElemento(i).getDate().getDayOfYear();
				hora = infraccionesFecha.darElemento(i).getDate().getHour();
				lista.enqueue(infraccionesFecha.darElemento(i));
			}

		}
		InfraccionesFechaHora dato = new InfraccionesFechaHora(actual.getDate(),infraccionesFecha.darElemento(infraccionesFecha.darTamano()-1).getDate(),lista);
		arbol.put(dato, lista);
		Queue<VOMovingViolations> nueva = new Queue<VOMovingViolations>();
		InfraccionesFechaHora prueba = new InfraccionesFechaHora(null, null, nueva);
		prueba.setValorTotal(valorInicial);
		InfraccionesFechaHora prueba2 = new InfraccionesFechaHora(null, null, nueva);
		prueba2.setValorTotal(valorFinal);
		return(Queue<InfraccionesFechaHora>) arbol.keys(prueba, prueba2);
	}

	/**
	 * Requerimiento 1C: Obtener  la informaci�n de  una  addressId dada
	 * @param  int addressID: Localizaci�n de la consulta.
	 * @return Objeto InfraccionesLocalizacion
	 */
	public InfraccionesLocalizacion consultarPorAddressId(int addressID)
	{
		Queue<VOMovingViolations>lista=new Queue<VOMovingViolations>();
		for(int i=0;i<infraccionesAddress.darTamano();i++)
		{
			if(infraccionesAddress.darElemento(i).getAddressIdi()==addressID)
			{
				lista.enqueue(infraccionesAddress.darElemento(i));
			}
			else if(infraccionesAddress.darElemento(i).getAddressIdi()>addressID)
			{
				return new InfraccionesLocalizacion(infraccionesAddress.darElemento(i-1).getXCoord(),infraccionesAddress.darElemento(i-1).getYCoord(),infraccionesAddress.darElemento(i-1).getLocation(),infraccionesAddress.darElemento(i-1).getAddressId(),infraccionesAddress.darElemento(i-1).getStreetSegId(),lista);		

			}
		}
		// TODO completar
		return null;
	}

	/**
	 * Requerimiento 2C: Obtener  las infracciones  en  un  rango de
	 * horas  [HH:MM:SS  inicial,HH:MM:SS  final]
	 * @param  LocalTime horaInicial: Hora  inicial del rango de b�squeda
	 * 		LocalTime horaFinal: Hora final del rango de b�squeda
	 * @return Objeto InfraccionesFranjaHorariaViolationCode
	 */
	public InfraccionesFranjaHorariaViolationCode consultarPorRangoHoras(LocalTime horaInicial, LocalTime horaFinal)
	{
		ArregloDinamico<VOMovingViolations> aux=new ArregloDinamico<VOMovingViolations>(250);
		Queue<VOMovingViolations> violacionesFecha = new Queue<VOMovingViolations>();
		for( int i=0; i < infraccionesHora.darTamano(); i++)
		{
			if(infraccionesHora.darElemento(i).getDate().toLocalTime().compareTo(horaInicial)>=0 && datos.darElemento(i).getDate().toLocalTime().compareTo(horaFinal)<=0 )
			{
				aux.agregar(infraccionesHora.darElemento(i));
				violacionesFecha.enqueue(datos.darElemento(i));	
			}
			else if(infraccionesHora.darElemento(i).getDate().toLocalTime().compareTo(horaFinal)>0)
			{
				Sort.sortM(aux, new VOMovingViolations.comparatorViolationCode());
				Queue<InfraccionesViolationCode> violacionesCodigo = new Queue<InfraccionesViolationCode>();
				Queue<VOMovingViolations> violacionesTemp = new Queue<VOMovingViolations>();

				VOMovingViolations actual=aux.darElemento(0);
				for(int j=0;j<aux.darTamano();j++)
				{
					if(aux.darElemento(j).getViolationCode().equals(actual.getViolationCode()))
					{
						violacionesTemp.enqueue(aux.darElemento(j));
					}
					else
					{
						violacionesCodigo.enqueue(new InfraccionesViolationCode(actual.getViolationCode(),violacionesTemp));
						violacionesTemp=new Queue<VOMovingViolations>();
						violacionesTemp.enqueue(aux.darElemento(j));
						actual=aux.darElemento(j);
					}
				}
				violacionesCodigo.enqueue(new InfraccionesViolationCode(actual.getViolationCode(),violacionesTemp));
				return new InfraccionesFranjaHorariaViolationCode(horaInicial,horaFinal,violacionesFecha,violacionesCodigo);
			}
		}
		return null;
	}
	/**
	 * Requerimiento 3C: Obtener  el  ranking  de  las  N localizaciones geogr�ficas
	 * (Xcoord,  Ycoord)  con  la mayor  cantidad  de  infracciones.
	 * @param  int N: Numero de las localizaciones con mayor n�mero de infracciones
	 * @return Cola de objetos InfraccionesLocalizacion
	 */
	public IQueue<LocalizacionInfracciones2> rankingNLocalizaciones(int N)
	{
		// TODO completar
			VOMovingViolations actual = infraccionesLocalizacion.darElemento(0);
			Queue<VOMovingViolations> violaciones = new Queue<VOMovingViolations>();
			violaciones.enqueue(actual);
			MaxHeapCP<LocalizacionInfracciones2> cola = new MaxHeapCP<LocalizacionInfracciones2>(3000);

			for(int i = 1; i<infraccionesLocalizacion.darTamano(); i++)
			{
				if(actual.compareTo(infraccionesLocalizacion.darElemento(i))==0)
				{
					violaciones.enqueue(infraccionesLocalizacion.darElemento(i));
				}
				else
				{
					LocalizacionInfracciones2 dato = new LocalizacionInfracciones2(actual.getXCoord(), actual.getYCoord(), actual.getLocation(), actual.getAddressId(), actual.getStreetSegId(), violaciones);
					cola.agregar(dato);
					actual = infraccionesLocalizacion.darElemento(i);
					violaciones = new Queue<VOMovingViolations>();
				}
			}	
			Queue<LocalizacionInfracciones2> respuesta = new Queue<LocalizacionInfracciones2>();
			for(int i = 0 ;i < N; i++)
			{
				respuesta.enqueue(cola.delMax());
			}
			return respuesta;		
				
	}
	/**
	 * Requerimiento 4C: Obtener la  informaci�n  de  los c�digos (ViolationCode) ordenados por su numero de infracciones.
	 * @return Contenedora de objetos InfraccionesViolationCode.
	  // TODO Definir la estructura Contenedora
	 */
	public MaxHeapCP<InfraccionesViolationCode> ordenarCodigosPorNumeroInfracciones()
	{
		// TODO completar
		// TODO Definir la Estructura Contenedora
		if(codes!=null)
		{
			return codes;
		}
		else
		{
			VOMovingViolations.comparatorViolationCode  comparator = new VOMovingViolations.comparatorViolationCode();
			Sort.sortM(datos, comparator);
			String code = datos.darElemento(0).getViolationCode();
			Queue<VOMovingViolations> violaciones = new Queue<VOMovingViolations>();
			MaxHeapCP<InfraccionesViolationCode> respuesta = new MaxHeapCP<InfraccionesViolationCode>(1000);
			violaciones.enqueue(datos.darElemento(0));
			for(int i = 1; i < datos.darTamano(); i++)
			{
				if(datos.darElemento(i).getViolationCode().equals(code))
				{
					violaciones.enqueue(datos.darElemento(i));	
				}
				else
				{

					InfraccionesViolationCode dato = new InfraccionesViolationCode(code, violaciones);
					respuesta.agregar(dato);
					code = datos.darElemento(i).getViolationCode();
					violaciones = new Queue<VOMovingViolations>();
					violaciones.enqueue(datos.darElemento(i));
				}
			}
			return respuesta;
		}

	}


}